import { FfiveCmsAdminPage } from './app.po';

describe('ffive-cms-admin App', () => {
  let page: FfiveCmsAdminPage;

  beforeEach(() => {
    page = new FfiveCmsAdminPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
