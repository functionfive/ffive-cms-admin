const path = require('path')
const fs = require('fs')
const rimraf = require('rimraf')

if (fs.existsSync('./dist/pkg'))
    rimraf.sync('./dist/pkg');

fs.mkdirSync('./dist/pkg')

// INDEX.HTML
fs.writeFileSync('./dist/pkg/index.html.handler.json', JSON.stringify({
    _id: 'CMSAdmin.Index',
    type: 'HTML',
    name: 'CMSAdmin.Index',
    module: 'CMSAdmin',
    routeMatcher: '^/cms-admin$'
}))
fs.writeFileSync('./dist/index.html', fs.readFileSync('./dist/index.html').toString().replace('<base href="/">','<base href="' + process.argv[2] + '">'))
fs.writeFileSync('./dist/pkg/index.html.source.json', JSON.stringify({
    content: fs.readFileSync('./dist/index.html').toString(),
    created_at: new Date(),
    handlerId: 'CMSAdmin.Index',
    version_number: 1
}))

let files = fs.readdirSync('./dist').filter(f => f.indexOf('.js') > -1).map(f=>'./dist/' + f)
files.push('./dist/favicon.ico')

files.map((file) => {
    fs.writeFileSync(`./dist/pkg/${path.basename(file)}.handler.json`, JSON.stringify({
        _id: `CMSAdmin.${path.basename(file)}`,
        type: file.indexOf('.html') > -1 ? 'HTML' : file.indexOf('.map') > -1 ? 'SOURCEMAP' : file.indexOf('.js') > -1 ? 'ES5' : file.indexOf('.ico') > -1 ? 'FAVICON' : '',
        name: `CMSAdmin.${path.basename(file)}`,
        module: 'CMSAdmin',
        routeMatcher: `^/cms-admin/${path.basename(file)}$`
    }))
    fs.writeFileSync(`./dist/pkg/${path.basename(file)}.source.json`, JSON.stringify({
        content: fs.readFileSync(file).toString(),
        created_at: new Date(),
        handlerId: `CMSAdmin.${path.basename(file)}`,
        version_number: 1
    }))
})

fs.writeFileSync(`./dist/pkg/install.js`, `
const path = require('path')
const fs = require('fs')

module.exports = async (db, override) => {

    // Upsert all handlers
    let handlers = fs.readdirSync(__dirname).filter(f => f.indexOf('.handler.json') > -1)
    
    if (override) {
        await db.collection('handlers').deleteMany({module: {$in: handlers.map(h => h.module)}})
    }

    for (let handler of handlers) {
        console.log('Attempting to load handler ' + handler)
        let data = require(path.join(__dirname, handler))

        await db.collection('handlers').update(data, data, {upsert: true})
        console.log(\`UPSERT HANDLER: \${data.name}\`)
    }

    // Insert all source as new versions
    let sources = fs.readdirSync(__dirname).filter(f => f.indexOf('.source.json') > -1)

    for (let src of sources) {
        console.log('Attempting to load src ' + src)
        let data = require(path.join(__dirname, src))

        if (override) {
            await db.collection('sources').deleteMany({handlerId: data.handlerId})
        }

        let head = (await db.collection('sources').find({handlerId: data.handlerId}).sort({version_number: -1}).limit(1).toArray())[0]
        if (head) {
            data.version_number = parseInt(head.version_number) + 1
        }
        data.created_at = new Date()

        await db.collection('sources').update(data, data, {upsert: true})
        console.log(\`UPSERT SOURCE: \${data.handlerId} v\${data.version_number}\`)
    }

    console.log('DONE')
}



`)