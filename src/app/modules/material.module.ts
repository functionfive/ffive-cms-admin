import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdButtonModule, 
  MdCheckboxModule,
  MdCardModule, 
  MdSidenavModule, 
  MdInputModule, 
  MdSlideToggleModule,
  MdListModule,
  MdIconModule,
  MdToolbarModule,
  MdTableModule,
  MdTabsModule,
  MdSelectModule
} from '@angular/material';

@NgModule({
  imports: [BrowserAnimationsModule, MdButtonModule, MdCheckboxModule, MdCardModule, MdSidenavModule, MdSlideToggleModule, MdInputModule, MdListModule, MdIconModule, MdToolbarModule, MdTableModule, MdTabsModule, MdSelectModule],
  exports: [BrowserAnimationsModule, MdButtonModule, MdCheckboxModule, MdCardModule, MdSidenavModule, MdSlideToggleModule, MdInputModule, MdListModule, MdIconModule, MdToolbarModule, MdTableModule, MdTabsModule, MdSelectModule],
})
export class MaterialModule { }