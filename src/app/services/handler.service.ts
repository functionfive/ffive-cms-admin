import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';
import '../dates'

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class HandlersService {

  constructor(private http: Http, private auth: AuthService) { }

  getHandler(id: string): Promise<HandlerDTO> {
    return this.http.get(`${environment.apiUrl}/api/handlers/${id}`,
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <HandlerDTO>r.json()).toPromise();
  }

  getSourceHead(handlerId: string) {
    return this.http.get(`${environment.apiUrl}/api/handlers/${handlerId}/source/head`,
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <HandlerSourceDTO>r.json()).toPromise();
  }

  getHandlers(): Promise<Handler[]> {
    return this.http.get(`${environment.apiUrl}/api/handlers`,
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <Handler[]>r.json()).toPromise();
  }

  saveHandler(handler: HandlerDTO) {
    return this.http[handler._id ? 'put' : 'post'](
        `${environment.apiUrl}/api/handlers${handler._id ? '/' + handler._id : ''}`, 
        handler, 
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <HandlerDTO>r.json()).toPromise();
  }

}

export interface HandlerDTO extends Handler {
  error?: string
  success: boolean
}

export interface HandlerSourceDTO extends HandlerSource {
  error?: string
  success: boolean
}

export interface HandlerSource {
  _id: string
  content: string
  module: string
  created_at: string
  handlerId: string
  version_number: number
}

export interface Handler {
  _id: string
  module: string
  type: 'SOURCEMAP' | 'FAVICON' | 'HTML' | 'ES5' | 'CSS' | 'CUSTOM'
  name: string
  routeMatcher: string 
}