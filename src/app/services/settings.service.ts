import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';
import '../dates'

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class SettingsService {

  constructor(private http: Http, private auth: AuthService) { }

  getSetting(id: string): Promise<SettingDTO> {
    return this.http.get(`${environment.apiUrl}/api/settings/${id}`,
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <SettingDTO>r.json()).toPromise();
  }  

  saveSetting(setting: SettingDTO) {
    return this.http[setting._id ? 'put' : 'post'](
        `${environment.apiUrl}/api/settings${setting._id ? '/' + setting._id : ''}`, 
        setting, 
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <SettingDTO>r.json()).toPromise();
  }

}

export interface SettingDTO extends Setting {
  error?: string
  success: boolean
}

export interface Setting {
  _id: string
  module: string
  values: any | Array<SettingsPage>
}

export interface SettingsPage {
  url: string
  label: string
}
