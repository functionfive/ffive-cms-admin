import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from '../../environments/environment'
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UsersService {

  constructor(private http: Http, private auth: AuthService) { }

  getUsers() {
    return this.http.get(`${environment.apiUrl}/api/users`, { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})})
      .map(r => <User[]>r.json());
  }

  getUser(id: string) {
    return this.http.get(`${environment.apiUrl}/api/users/${id}`, { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})})
      .map(r => <User>r.json())
      .toPromise();
  }

  saveUser(user: User): Promise<UserDTO> {
    return this.http[user._id ? 'put' : 'post'](
        `${environment.apiUrl}/api/users${user._id ? '/' + user._id : ''}`, 
        user, 
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <UserDTO>r.json()).toPromise();
  }

  resetPasswordStart(id: string): Promise<ResetDTO> {
    return this.http.post(
        `${environment.apiUrl}/api/users${id ? '/' + id : ''}/password_reset_start`, {},
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <ResetDTO>r.json()).toPromise();
  }

  resetPasswordComplete(dto: ResetDTO): Promise<ResetDTO> {
    return this.http.post(
        `${environment.apiUrl}/api/users${dto._id ? '/' + dto._id : ''}/password_reset_complete`, dto,
        { headers: new Headers({'Authorization': 'JWT ' + this.auth.token})}
      ).map(r => <ResetDTO>r.json()).toPromise();
  }

}

export interface ResetDTO {
  _id?: string
  email: string
  password: string
  confirm_password: string
  token: string
  success?: boolean
  error?: string
}

export interface User {
  email: string
  first_name: string
  last_name: string
  tags: string[]
  _id: string
}

export interface UserDTO {
  user: User
  error?: string
  errors?: {[field:string]: string}
  success?: boolean
}
