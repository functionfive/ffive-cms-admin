import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import '../dates'

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class AuthService {

  constructor(private http: Http) { }

  logout() {
    localStorage.removeItem('auth')
    return Promise.resolve()
  }

  get token() {
    return localStorage.getItem('auth')
  }

  authenticate(username: string, password: string) {
    return this.http.post(`${environment.apiUrl}/api/users/authorize`, {email: username, password})
      .map((resp) => <AuthResponse>resp.json())
      .map((authResponse) => {
        if (authResponse.success) {
          localStorage.setItem('auth', authResponse.token)
        }
        return authResponse
      }).toPromise();
  }

  get authenticated() {
    let payload = this.token
    if (!payload) 
      return false
    return new Date(this.profile.exp * 1000) >= new Date()
  }

  get profile() {
    return JSON.parse(atob(this.token.split('.')[1]))
  }

}

export interface AuthResponse {
  error?: string
  token?: string
  details?: string
  success: boolean
}

export interface SessionProfile {

}
