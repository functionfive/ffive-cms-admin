import { Component, OnInit } from '@angular/core';
import { HandlersService, Handler } from '../../services/handler.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-handlers',
  templateUrl: './handlers.component.html',
  styleUrls: ['./handlers.component.scss']
})
export class HandlersComponent implements OnInit {
  handlers: Array<Handler> = []
  moduleFilter: string 
  
  constructor(private svc: HandlersService, private router: Router) { }

  async ngOnInit() {
    this.handlers = await this.svc.getHandlers()
  }

  get filteredHandlers() {
    return this.moduleFilter && this.handlers ? this.handlers.filter(h => h.module === this.moduleFilter) : this.handlers || []
  }

  get modules() { 
    return (this.handlers || []).map(h => h.module).filter((v, i, s) => s.indexOf(v) === i)
  }

}
