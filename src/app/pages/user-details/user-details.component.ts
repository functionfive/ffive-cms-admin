import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { UsersService, User, UserDTO, ResetDTO } from '../../services/users.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  user: User
  userResponse: UserDTO
  resetResponse: ResetDTO
  loading: boolean
  tags: {[tag:string]: boolean} = {}

  constructor(private toastr: ToastsManager, private userService: UsersService, private route: ActivatedRoute, private vcr: ViewContainerRef) {
      this.toastr.setRootViewContainerRef(vcr);
  }

  toggleTag(tag) {
    if (this.user.tags.indexOf(tag) > -1) {
      console.log('Removing tag')
      this.user.tags = this.user.tags.filter(t => t !== tag)
    } else {
      console.log('Adding tag')
      this.user.tags.push(tag)
    }
  }

  async ngOnInit() {
    if (this.route.snapshot.paramMap.get('id') === 'new') {
      this.user = {_id: null, tags: [], email: '', first_name: '', last_name: ''}
      this.user.tags.forEach(t => this.tags[t] = true)
    } else {
      this.loading = true
      this.user = await this.userService.getUser(this.route.snapshot.paramMap.get('id'))
      this.user.tags.forEach(t => this.tags[t] = true)
      this.loading = false
    }
  }

  async resetPassword() {
    try {
      this.loading = true
      this.resetResponse = await this.userService.resetPasswordStart(this.user._id)
      this.loading = false
      this.toastr.success('Password reset email sent!')
    } catch (err) {
      this.loading = false
      switch(err.status) {
        case 406:
          this.toastr.error(err.json().error, 'Password reset not sent')
          break;
        default:
          this.toastr.error(err.json().error, 'Password reset not sent')
          break;
      }
      
    }
  }

  async save() {
    try {
      this.user.tags = Object.keys(this.tags).filter(k => this.tags[k])
      this.loading = true
      this.userResponse = await this.userService.saveUser(this.user)
      this.loading = false
      this.toastr.success('User Saved')
    } catch (err) {
      this.userResponse = err.json()
      this.loading = false
      switch(err.status) {
        case 406:
          this.toastr.error(err.json().error, 'User Not Saved')
          break;
        default:
          this.toastr.error(err.json().error, 'User Not Saved')
          break;
      }
      
    }
  }

}
