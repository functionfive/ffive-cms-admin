import { Component, OnInit } from '@angular/core';
import { AuthService, SessionProfile } from '../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  profile: SessionProfile;
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.profile = this.auth.profile
  }

}
