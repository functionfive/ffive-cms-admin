import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, AuthResponse, SessionProfile } from '../../services/auth.service';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public user: {username?: string, password?: string} = {};
  public authResponse?: AuthResponse;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  async submit() {
    try {
      this.authResponse = await this.auth.authenticate(this.user.username, this.user.password)
      if (this.authResponse.success) {
        this.router.navigate(['/dashboard']);
      }
    } catch(err) {
      this.authResponse = err.json()
      return Observable.throw(this.authResponse.error)
    }
  }

}
