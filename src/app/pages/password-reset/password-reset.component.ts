import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { UsersService, User, UserDTO, ResetDTO } from '../../services/users.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {
  token: string
  loading: boolean
  resetDTO: ResetDTO

  constructor(private toastr: ToastsManager, private userService: UsersService, private route: ActivatedRoute, vcr: ViewContainerRef) {
      this.toastr.setRootViewContainerRef(vcr);
  }

  async ngOnInit() {
    this.loading = true
    this.resetDTO = JSON.parse(atob(this.route.snapshot.paramMap.get('token').split('.')[1]))
    this.resetDTO.token = this.route.snapshot.paramMap.get('token')
    this.loading = false
  }

  async resetPassword() {
    try {
      this.loading = true
      this.resetDTO = await this.userService.resetPasswordComplete(this.resetDTO)
      this.loading = false
      this.toastr.success('Password reset email sent!')
    } catch (err) {
      this.loading = false
      switch(err.status) {
        case 406:
          this.toastr.error(err.json().error, 'Password reset not sent')
          break;
        default:
          this.toastr.error(err.json().error, 'Password reset not sent')
          break;
      }
      
    }
  }

}
