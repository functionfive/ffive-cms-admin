import { Component, OnInit } from '@angular/core';
import { UsersService, User } from '../../services/users.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: Array<User> = []

  constructor(private svc: UsersService, private router: Router) { }

  ngOnInit() {
    this.svc.getUsers().subscribe(users => this.users = users)
  }

}
