import { Component, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser'
import { SettingsService, SettingsPage, SettingDTO, Setting } from '../../services/settings.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {
  settingsPages: SettingDTO
  currentSetting: SettingDTO
  settingResponse: SettingDTO
  
  constructor(private toastr: ToastsManager, 
    private settingsService: SettingsService, 
    private route: ActivatedRoute, 
    private sanitizer: DomSanitizer, 
    private vcr: ViewContainerRef,
    private router: Router) { 
    this.toastr.setRootViewContainerRef(vcr);
    this.router.events.filter(e => e instanceof NavigationEnd).subscribe(() => this.load());
  }

  async load() {
    this.settingsPages = await this.settingsService.getSetting('SETTINGS_PAGES')
    this.currentSetting = await this.settingsService.getSetting(this.currentSettingsPage.label)
  }

  async saveSetting() {
    try {
      this.settingResponse = await this.settingsService.saveSetting(this.currentSetting)
      this.toastr.success('Setting Saved!')
    } catch(err) {
      this.settingResponse = err.json()
      this.toastr.error(err.json().error, 'Setting failed to update')
    }
  }

  get currentSettingsPage() {
    return (this.settingsPages && this.settingsPages.values ? this.settingsPages.values : []).find(p => p.label.toLowerCase() === (this.route.snapshot.paramMap.get('id') || 'Admin').toLowerCase())
  }

  get sanitizedPageUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.currentSettingsPage ? this.currentSettingsPage.url : '')
  }

  get iframeMode() {
    return !(this.currentSettingsPage && ['admin', 'smtp'].indexOf(this.currentSettingsPage.label.toLowerCase()) > -1)
  }

}
