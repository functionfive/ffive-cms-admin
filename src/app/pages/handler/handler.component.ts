import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { HandlersService, Handler, HandlerDTO, HandlerSource, HandlerSourceDTO } from '../../services/handler.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({
  selector: 'app-handler',
  templateUrl: './handler.component.html',
  styleUrls: ['./handler.component.scss']
})
export class HandlerComponent implements OnInit {
  handler: Handler
  handlerResponse: HandlerDTO
  loading: boolean
  text: string
  source: HandlerSourceDTO
  handlerTypes: string[] = ['HTML', 'EJS', 'ES5', 'CSS', 'SOURCEMAP', 'CUSTOM', 'NODE']

  constructor(private toastr: ToastsManager, private handlerService: HandlersService, private route: ActivatedRoute, private vcr: ViewContainerRef) {
      this.toastr.setRootViewContainerRef(vcr);
  }
  async ngOnInit() {
    if (this.route.snapshot.paramMap.get('id') === 'new') {
      this.handler = {_id: null, module: '', type: 'HTML', name: '', routeMatcher: ''}
    } else {
      this.loading = true
      this.handler = await this.handlerService.getHandler(this.route.snapshot.paramMap.get('id'))
      this.source = await this.handlerService.getSourceHead(this.route.snapshot.paramMap.get('id'))
      this.loading = false
    }
  }

  async save() {
    try {
      this.loading = true
      this.handlerResponse = await this.handlerService.saveHandler(<HandlerDTO>this.handler)
      this.loading = false
      this.toastr.success('Handler Saved')
    } catch (err) {
      this.handlerResponse = err.json()
      this.loading = false
      switch(err.status) {
        case 406:
          this.toastr.error(err.json().error, 'Handler Not Saved')
          break;
        default:
          this.toastr.error(err.json().error, 'Handler Not Saved')
          break;
      }
      
    }
  }

}
