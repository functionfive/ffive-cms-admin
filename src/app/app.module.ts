import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './modules/material.module';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { RequiresAuthGuard } from './guards/requires-auth.guard';
import { RequiresNoAuthGuard } from './guards/requires-no-auth.guard';
import { AuthService } from './services/auth.service';
import { UsersService } from './services/users.service';
import { UsersComponent } from './pages/users/users.component';
import { UserDetailsComponent } from './pages/user-details/user-details.component';
import { ToastModule, ToastsManager, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { PasswordResetComponent } from './pages/password-reset/password-reset.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { SettingsService } from './services/settings.service';
import { HandlersService } from './services/handler.service';
import { HandlersComponent } from './pages/handlers/handlers.component';
import { HandlerComponent } from './pages/handler/handler.component';
import { AceEditorModule } from 'ng2-ace-editor'

export class CustomOption extends ToastOptions {
  animate = 'flyRight'; // you can override any options available
  positionClass = 'toast-bottom-right'
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PageNotFoundComponent,
    UsersComponent,
    UserDetailsComponent,
    PasswordResetComponent,
    SettingsComponent,
    HandlersComponent,
    HandlerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    ToastModule,
    AppRoutingModule, 
    AceEditorModule,
    ToastModule.forRoot()
  ],
  providers: [
    AuthService,
    UsersService,
    HandlersService,
    SettingsService,
    RequiresAuthGuard,
    RequiresNoAuthGuard,
    ToastsManager,
    {provide: ToastOptions, useClass: CustomOption}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
 