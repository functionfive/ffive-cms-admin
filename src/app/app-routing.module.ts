import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component'
import { DashboardComponent } from './pages/dashboard/dashboard.component'
import { UsersComponent } from './pages/users/users.component'
import { UserDetailsComponent } from './pages/user-details/user-details.component'
import { LoginComponent } from './pages/login/login.component'
import { SettingsComponent } from './pages/settings/settings.component'
import { HandlersComponent } from './pages/handlers/handlers.component'
import { HandlerComponent } from './pages/handler/handler.component'
import { PasswordResetComponent } from './pages/password-reset/password-reset.component'
import { RequiresAuthGuard } from './guards/requires-auth.guard';
import { RequiresNoAuthGuard } from './guards/requires-no-auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [RequiresNoAuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [RequiresAuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [RequiresAuthGuard] },
  { path: 'settings', component: SettingsComponent, canActivate: [RequiresAuthGuard] },
  { path: 'handlers', component: HandlersComponent, canActivate: [RequiresAuthGuard] },
  { path: 'settings/:id', component: SettingsComponent, canActivate: [RequiresAuthGuard] },
  { path: 'users/:id', component: UserDetailsComponent, canActivate: [RequiresAuthGuard] },
  { path: 'handlers/:id', component: HandlerComponent, canActivate: [RequiresAuthGuard] },
  { path: 'users/:id/password_reset/:token', component: PasswordResetComponent },
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
