import { Component, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { MdCard, MdSidenavContainer } from '@angular/material';
import { environment } from '../environments/environment';
import * as browser from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(private location: Location, 
    private auth: AuthService, 
    private router: Router,
    @Inject(browser.DOCUMENT) private document) {

  }

  ngOnInit(): void {
    let bases = this.document.getElementsByTagName('base');

    if (bases.length > 0) {
      bases[0].setAttribute('href', environment.baseHref);

    }
  }

  logout() {
    this.auth.logout().then(() => this.router.navigate(['/login']))
  }

  get sidebarHidden() {
    let list = ["/login"],
        route = this.location.path();
    return (list.indexOf(route) === -1 && route.indexOf('password_reset') === -1);
  }
}
